// EVENT 0071
Name "Skill: Play 311-320"

ChangeVariable(11,11,0,2,1,5)
If(1,100,0,311,0)
 If(1,11,0,1,0)
  ShowMessageFace("trickfairy_fc1",0,0,2,1)
  ShowMessage("【シシリー】")
  ShowMessage("楽しく踊ろうよ～♪")
  355("interrupt_skill(2032)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("trickfairy_fc1",0,0,2,2)
  ShowMessage("【シシリー】")
  ShowMessage("えへへへっ……♪")
  ShowMessageFace("",0,0,2,3)
  ShowMessage("シシリーはくすくす笑っている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("trickfairy_fc1",0,0,2,4)
  ShowMessage("【シシリー】")
  ShowMessage("おひるねするね……♪")
  313(0,311,0,11)
  ShowMessageFace("",0,0,2,5)
  ShowMessage("シシリーは居眠りしている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("trickfairy_fc1",0,0,2,6)
  ShowMessage("【シシリー】")
  ShowMessage("この子、お友達だよ♪")
  ShowMessageFace("",0,0,2,7)
  ShowMessage("シシリーからの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(450,0,0,1)
  ShowMessageFace("",0,0,2,8)
  ShowMessage("「ちぃぱっぱ」を手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("trickfairy_fc1",0,0,2,9)
  ShowMessage("【シシリー】")
  ShowMessage("妖精の踊りだよ～♪")
  355("interrupt_skill(2037)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,312,0)
 If(1,11,0,1,0)
  ShowMessageFace("amazoneself_fc1",0,0,2,10)
  ShowMessage("【サン】")
  ShowMessage("せいっ！　せいっ！")
  355("interrupt_skill(3287)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("amazoneself_fc1",0,0,2,11)
  ShowMessage("【サン】")
  ShowMessage("せいっ！　せいっ！")
  355("interrupt_skill(3291)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("amazoneself_fc1",0,0,2,12)
  ShowMessage("【サン】")
  ShowMessage("大地の精よ、私達にご加護を……！")
  ShowMessageFace("",0,0,2,13)
  ShowMessage("サンは大地に祈っている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("amazoneself_fc1",1,0,2,14)
  ShowMessage("【サン】")
  ShowMessage("これを食べて、力をつけな！")
  ShowMessageFace("",0,0,2,15)
  ShowMessage("サンからの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(329,0,0,1)
  ShowMessageFace("",0,0,2,16)
  ShowMessage("「肉」を手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("amazoneself_fc1",0,0,2,17)
  ShowMessage("【サン】")
  ShowMessage("ふんっ！")
  355("interrupt_skill(3294)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,313,0)
 If(1,11,0,1,0)
  ShowMessageFace("carbuncle_fc1",0,0,2,18)
  ShowMessage("【カルカナ】")
  ShowMessage("退屈ですね……")
  ShowMessageFace("",0,0,2,19)
  ShowMessage("カルカナはぼーっとしている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("carbuncle_fc1",0,0,2,20)
  ShowMessage("【カルカナ】")
  ShowMessage("お腹が減りました……")
  ShowMessageFace("",0,0,2,21)
  ShowMessage("カルカナは食料を盗み食いしている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("carbuncle_fc1",1,0,2,22)
  ShowMessage("【カルカナ】")
  ShowMessage("ニコッ♪")
  ShowMessageFace("",0,0,2,23)
  ShowMessage("カルカナはにっこり微笑んだ！")
  ShowMessage("しかし何も起きなかった……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("carbuncle_fc1",1,0,2,24)
  ShowMessage("【カルカナ】")
  ShowMessage("これ、新鮮で美味しいですよ♪")
  ShowMessageFace("",0,0,2,25)
  ShowMessage("……からの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(306,0,0,1)
  ShowMessageFace("",0,0,2,26)
  ShowMessage("「きゅうり」を手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("carbuncle_fc1",0,0,2,27)
  ShowMessage("【カルカナ】")
  ShowMessage("皆を守りましょう……！")
  355("interrupt_skill(1717)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,314,0)
 If(1,11,0,1,0)
  ShowMessageFace("gigantic_fc1",1,0,2,28)
  ShowMessage("【ジェイム】")
  ShowMessage("今日のごはんは、何かな～？")
  ShowMessageFace("",0,0,2,29)
  ShowMessage("ジェイムはわくわくしている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("gigantic_fc1",0,0,2,30)
  ShowMessage("【ジェイム】")
  ShowMessage("地上絵を描くぞ～！")
  ShowMessageFace("",0,0,2,31)
  ShowMessage("ジェイムは地面に落書きをしている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("gigantic_fc1",1,0,2,32)
  ShowMessage("【ジェイム】")
  ShowMessage("ぐっすり……")
  313(0,314,0,11)
  ShowMessageFace("",0,0,2,33)
  ShowMessage("ジェイムは居眠りしている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("gigantic_fc1",0,0,2,34)
  ShowMessage("【ジェイム】")
  ShowMessage("ぽかぽかするな……")
  ShowMessageFace("",0,0,2,35)
  ShowMessage("ジェイムは地面に寝そべって日光浴している……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("gigantic_fc1",0,0,2,36)
  ShowMessage("【ジェイム】")
  ShowMessage("ずずーん！")
  355("interrupt_skill(2993)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,315,0)
 If(1,11,0,1,0)
  ShowMessageFace("queenfairy_fc1",0,0,2,37)
  ShowMessage("【エアリー】")
  ShowMessage("うふふふっ……")
  ShowMessageFace("",0,0,2,38)
  ShowMessage("エアリーはくすくす笑っている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("queenfairy_fc1",0,0,2,39)
  ShowMessage("【エアリー】")
  ShowMessage("果ててもらいましょう……")
  355("interrupt_skill(2042)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("queenfairy_fc1",2,0,2,40)
  ShowMessage("【エアリー】")
  ShowMessage("女王たるもの、火遊びも一流……")
  ShowMessage("……ひゃぁぁぁぁ！！")
  313(0,315,0,13)
  ShowMessageFace("",0,0,2,41)
  ShowMessage("エアリーは火遊びをした……")
  ShowMessage("失敗し、自分の体に引火してしまった！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("queenfairy_fc1",0,0,2,42)
  ShowMessage("【エアリー】")
  ShowMessage("みんな、がんばって下さいね……♪")
  355("interrupt_skill(3300)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("queenfairy_fc1",0,0,2,43)
  ShowMessage("【エアリー】")
  ShowMessage("妖精の踊り、見てもらいましょう……")
  355("interrupt_skill(2037)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,316,0)
 If(1,11,0,1,0)
  ShowMessageFace("queenelf_fc1",0,0,2,44)
  ShowMessage("【フレイヤ】")
  ShowMessage("……………………")
  ShowMessageFace("",0,0,2,45)
  ShowMessage("フレイヤは流し目を送った……")
  ShowMessage("しかし、睨んでいるようにしか見えない！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("queenelf_fc1",0,0,2,46)
  ShowMessage("【フレイヤ】")
  ShowMessage("女王たる者、音楽にも造詣があるのです……")
  355("interrupt_skill(3305)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("queenelf_fc1",0,0,2,47)
  ShowMessage("【フレイヤ】")
  ShowMessage("妖精とは違い、あまり踊りは得意ではないですが……")
  ShowMessageFace("",0,0,2,48)
  ShowMessage("フレイヤはゆかいなダンスを踊った！")
  ShowMessage("しかし何も起きなかった……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("queenelf_fc1",0,0,2,49)
  ShowMessage("【フレイヤ】")
  ShowMessage("これを差し上げましょう……")
  ShowMessageFace("",0,0,2,50)
  ShowMessage("フレイヤからの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(404,0,0,1)
  ShowMessageFace("",0,0,2,51)
  ShowMessage("「たいやき」を手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("queenelf_fc1",0,0,2,52)
  ShowMessage("【フレイヤ】")
  ShowMessage("はっ！")
  355("interrupt_skill(3294)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,317,0)
 If(1,11,0,1,0)
  ShowMessageFace("f_alraune_fc1",0,0,2,53)
  ShowMessage("【アリア】")
  ShowMessage("ららら～♪")
  355("interrupt_skill(3288)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("f_alraune_fc1",0,0,2,54)
  ShowMessage("【アリア】")
  ShowMessage("あははっ、楽し～い♪")
  ShowMessageFace("",0,0,2,55)
  ShowMessage("アリアは陽気に歌って踊っている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("f_alraune_fc1",0,0,2,56)
  ShowMessage("【アリア】")
  ShowMessage("がんばれ～！　ファイト！")
  355("interrupt_skill(3300)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("f_alraune_fc1",0,0,2,57)
  ShowMessage("【アリア】")
  ShowMessage("これ、あげる！")
  ShowMessageFace("",0,0,2,58)
  ShowMessage("アリアからの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(304,0,0,1)
  ShowMessageFace("",0,0,2,59)
  ShowMessage("「さくらんぼ」を手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("f_alraune_fc1",0,0,2,60)
  ShowMessage("【アリア】")
  ShowMessage("お花の香りだよ……♪")
  355("interrupt_skill(2922)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,318,0)
 If(1,11,0,1,0)
  ShowMessageFace("f_dryad_fc1",0,0,2,61)
  ShowMessage("【ラシェル】")
  ShowMessage("光合成日和ですね……")
  ShowMessageFace("",0,0,2,62)
  ShowMessage("ラシェルは地面に寝そべって日光浴している……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("f_dryad_fc1",2,0,2,63)
  ShowMessage("【ラシェル】")
  ShowMessage("ぐすっ……ぐすん……")
  ShowMessageFace("",0,0,2,64)
  ShowMessage("ラシェルは嘘泣きをした！")
  ShowMessage("しかし、誰も聞いていない……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("f_dryad_fc1",2,0,2,65)
  ShowMessage("【ラシェル】")
  ShowMessage("戦いなんて、やめませんか……？")
  ShowMessageFace("",0,0,2,66)
  ShowMessage("ラシェルは仲裁に入った！")
  ShowMessage("しかし誰も聞いていない……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("f_dryad_fc1",0,0,2,67)
  ShowMessage("【ラシェル】")
  ShowMessage("これ、栄養たっぷりですよ……")
  ShowMessageFace("",0,0,2,68)
  ShowMessage("ラシェルからの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(307,0,0,1)
  ShowMessageFace("",0,0,2,69)
  ShowMessage("「キャベツ」を手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("f_dryad_fc1",0,0,2,70)
  ShowMessage("【ラシェル】")
  ShowMessage("やーっ！")
  355("interrupt_skill(2904)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,319,0)
 If(1,11,0,1,0)
  ShowMessageFace("walraune_fc1",3,0,2,71)
  ShowMessage("【ベラドンナ】")
  ShowMessage("ふふふっ……")
  ShowMessageFace("",0,0,2,72)
  ShowMessage("ベラドンナは悪巧みをしている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("walraune_fc1",3,0,2,73)
  ShowMessage("【ベラドンナ】")
  ShowMessage("妖花のポーズ！")
  ShowMessageFace("",0,0,2,74)
  ShowMessage("ベラドンナは決めポーズを繰り出した！")
  ShowMessage("しかし誰も見ていない……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("walraune_fc1",3,0,2,75)
  ShowMessage("【ベラドンナ】")
  ShowMessage("そろそろ、寝ようかな……")
  313(0,319,0,11)
  ShowMessageFace("",0,0,2,76)
  ShowMessage("ベラドンナは居眠りしている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("walraune_fc1",3,0,2,77)
  ShowMessage("【ベラドンナ】")
  ShowMessage("これ、食べなさい。")
  ShowMessageFace("",0,0,2,78)
  ShowMessage("ベラドンナからの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(346,0,0,1)
  ShowMessageFace("",0,0,2,79)
  ShowMessage("「あんぱん」を手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("walraune_fc1",3,0,2,80)
  ShowMessage("【ベラドンナ】")
  ShowMessage("花の香りでとろけなさい……！")
  355("interrupt_skill(2921)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,320,0)
 If(1,11,0,1,0)
  ShowMessageFace("dryad_fc1",1,0,2,81)
  ShowMessage("【ドリア】")
  ShowMessage("がんばれ～♪　がんばれ～♪")
  355("interrupt_skill(2922)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("dryad_fc1",1,0,2,82)
  ShowMessage("【ドリア】")
  ShowMessage("ほわわ～……")
  ShowMessageFace("",0,0,2,83)
  ShowMessage("ドリアはぼんやりしている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("dryad_fc1",1,0,2,84)
  ShowMessage("【ドリア】")
  ShowMessage("楽しいですね～♪")
  ShowMessageFace("",0,0,2,85)
  ShowMessage("ドリアは陽気に歌って踊っている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("dryad_fc1",1,0,2,86)
  ShowMessage("【ドリア】")
  ShowMessage("これ、どうぞ♪")
  ShowMessageFace("",0,0,2,87)
  ShowMessage("ドリアからの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(394,0,0,1)
  ShowMessageFace("",0,0,2,88)
  ShowMessage("「スパゲティ」を手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("dryad_fc1",1,0,2,89)
  ShowMessage("【ドリア】")
  ShowMessage("いい匂いですよ～♪")
  355("interrupt_skill(2922)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
0()
